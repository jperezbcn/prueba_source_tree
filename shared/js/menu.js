var menuPrincipal;
var estructuraMenu;
var estructura;

$(document).ready(function () {
//    var slide = 0;
//    var presentation = 0;
    /**
     * Array que representa l'estructura del material.
     * id de les CLM Presentation
     * + el nom de l'arxiu ZIP de cada Key Message (sense el .zip) de cada presentació
     * */

    // #guia - escriu aquí l'estructura de la teva presentació
    estructura = [];

    estructura[0] = ["presentacio-portada-00", [
            "key-message-portada-000"
        ]];

    estructura[1] = ["presentacio-contingut-01", [
            "key-message-contingut-000",
            "key-message-contingut-001",
            "key-message-contingut-002"
        ]];


    estructura[6] = ["presentacio-popup-06", [
            "key-message-popup-000"
        ]];



    estructuraMenu = [
        {
            "text": "Portada",
            "classes": "miau tokoto tracata",
            "ic": "ic-casa",
            "goto": {
                "slide": "0",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "Demos",
            "ic": "ic-miau",
            "goto": false,
            "children": [
                {
                    "text": "Demo popups interns",
                    "ic": "ic-demo",
                    "goto": false,
//                            {
//                        "touch": true,
//                        "slide": "0",
//                        "presentation": "1",
//                        "camins":[
//                            {
//                                "num": 1,
//                                "slide": "0",
//                                "presentation":"0"
//                            },
//                            {
//                                "num": 2,
//                                "slide": "0",
//                                "presentation":"0"
//                            }
//                        ]
//                    }
                    "children": [
                        {
                            "text": "Pantalla imaginaria de prova",
                            "ic": "ic-demo-1-1",
                            "goto": {
                                "slide": "0",
                                "presentation": "1"
                            },
                            "children": false
                        },
                        {
                            "text": "Pantalla imaginaria de prova 2",
                            "ic": "ic-demo-1-2",
                            "goto": {
                                "slide": "0",
                                "presentation": "4"
                            },
                            "children": false
                        }
                    ]
                },
                {
                    "text": "Demo popup KM propi",
                    "ic": "ic-demo-2",
                    "goto": {
                        "touch": true,
                        "slide": "1",
                        "presentation": "1"
                    },
                    "children": false
                },
                {
                    "text": "Demo animacions",
                    "ic": "ic-demo-3",
                    "goto": {
                        "touch": true,
                        "slide": "2",
                        "presentation": "1"
                    },
                    "children": false
                }
            ]
        },
        {
            "text": "Element inventat",
            "ic": "ic-tokoto",
            "goto": false,
            "children": [
                {
                    "text": "Pantalla imaginaria de prova",
                    "ic": "ic-demo-1-1",
                    "goto": {
                        "slide": "0",
                        "presentation": "1"
                    },
                    "children": false
                },
                {
                    "text": "Pantalla imaginaria de prova 2",
                    "ic": "ic-demo-1-2",
                    "goto": {
                        "slide": "0",
                        "presentation": "4"
                    },
                    "children": false
                }
            ]
        },
        {
            "text": "Obre popup",
            "ic": "ic-demo-1-4",
            "goto": false,
            "children": false,
            "popup": "#popup_referencies"
        }
    ];
    // 2n paràmetre: id de l'element on s'inclourà el menú
    menuPrincipal = new Menu(estructuraMenu, 'mainmenu');

    // #guia - IMPORTANT! Comentar aquesta línia al pujar al Salesforce
    // Habilitar el mode Test durant el desenvolupament i per testejar i debugar funcions de veeva! 
//    edetailing.enableTestMode();

    edetailing.debugBox = document.getElementById('returned-results');

    // Opcional: modificar valors per defecte. Cal fer-ho abans d'inicialitzar l'edetailing.
    edetailing.changeOverlayDefault('opacity', 0.3);
    edetailing.changePopupDefault('temps', 300);
//    edetailing.changeTouchEventDefault('touchstart');

    // 2n paràmetre: id de l'element on s'inclourà el codi
    edetailing.initCodi('1612213322', 'np4');


    inicializar_navigate();

});